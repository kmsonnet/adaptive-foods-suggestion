// Change style of navbar on scroll
window.onscroll = function() {myFunction()};
function myFunction() {
    var navbar = document.getElementById("navbar");
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
        navbar.className = "w3-bar" + " w3-card-8" + " w3-animate-top" + " w3-light-grey";
    } else {
        navbar.className = navbar.className.replace(" w3-card-8 w3-animate-top w3-light-grey", " w3-white");
    }
}

// Used to toggle the menu on small screens when clicking on the menu button
function toggleFunction() {
    var x = document.getElementById("navSmall");
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else {
        x.className = x.className.replace(" w3-show", "");
    }
}

function showAndHide(show, hide){
    document.getElementById(show).style.display = "block";
    document.getElementById(hide).style.display = "none";
}


function calcuTotal(){
    var q = document.getElementById('quantity').value;
    document.getElementById('quan').innerHTML = q;
    var price = document.getElementById('price').innerHTML;
    price = parseInt(price);
    var total =  document.getElementById('total');
    total.innerHTML = price* parseInt(q);
}