# Adaptive Foods Suggestion

This is a website for food lovers. They can find foods according to their requirements. These requirements consist of taste, price, environment, location and so on. 
For example if you want to find spicy food withing 200 BDT in Gulshan area and environment must be comfortable for couples then this website is for you. You can also order food online. I used fuzzy inference system (FIS) to make the result more accurate.