         <?php
class Customer_Model extends CI_Model {

	function insertSignupData(){
		$fname = $this->input->post('fname');
		$lname = $this->input->post('lname');
		$password = $this->input->post('password');
		$username = $this->input->post('username');
		$password = md5($password);		
		$contact = $this->input->post('contact');		
		$address = $this->input->post('address');	

		$sql = "INSERT INTO customer (fname, lname, username, password, contact, address)
								VALUES ('$fname', '$lname', '$username', '$password', '$contact', '$address')";
		if($this->db->query($sql)){			
		   $this->session->set_userdata('cus_name', $username);		   
		   return true;
		}
		else return false;
	}

	function login(){
		$password = $this->input->post('password');
		$password = md5($password);
		$username = $this->input->post('username');

		$sql = "SELECT * FROM customer WHERE username = '$username' AND password = '$password'";
		if(($this->db->query($sql)->num_rows())>0){
			$this->session->set_userdata('cus_name', $username);		   	
			return true;
		}
		else 
			return false;
	}

	function logout(){		
		$this->session->unset_userdata('cus_name');		
		$this->session->unset_userdata('cus_err');
		$home = base_url();
		redirect($home);
	}

	function getTaste(){
		$get = "SELECT DISTINCT taste FROM foods";
		$query = $this->db->query($get);
		return $query->result_array();
	}

	function getEnvironment(){
		$get = "SELECT DISTINCT type FROM restaurent";
		$query = $this->db->query($get);
		return $query->result_array();
	}

	function getHistoryOfFood($username){
		$sql = "SELECT f.*, r.name as rname FROM foods f JOIN order_details o ON o.fid=f.fid
				JOIN restaurent r ON o.rid = r.rid WHERE o.username = '$username' AND o.state=0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

}