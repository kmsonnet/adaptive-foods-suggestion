<?php
class Restaurent_Model extends CI_Model {

	function insertSignupData(){
		$name = $this->input->post('name');
		$password = $this->input->post('password');
		$password = md5($password);
		$email = $this->input->post('email');
		$type = $this->input->post('type');
		$address = $this->input->post('address');
		$rid = $this->input->post('username');

		$sql = "INSERT INTO restaurent (name, password, email, type, address, rid)
								VALUES ('$name', '$password', '$email', '$type', '$address','$rid')";
		if($this->db->query($sql)){			
		   $this->session->set_userdata('res_name', $rid);		   
		   return true;
		}
		else return false;
	}

	function login(){
		$password = $this->input->post('password');
		$password = md5($password);
		$name = $this->input->post('name');

		$sql = "SELECT * FROM restaurent WHERE rid = '$name' AND password = '$password'";
		if(($this->db->query($sql)->num_rows())>0){
			$this->session->set_userdata('res_name', $name);		   	
			return true;
		}
		else 
			return false;
	}

	function logout(){		
		$this->session->unset_userdata('res_name');		
		$this->session->unset_userdata('res_err');
		$home = base_url();
		redirect($home);
	}

	function insertFoods(){
		$name = $this->input->post('name');
		$taste = $this->input->post('taste');
		$price = $this->input->post('price');
		$rid = $this->session->userdata('res_name');

		if(!empty($_FILES['img']['name'])){
			$config['upload_path'] ='img/food/';
			$config['max_size'] = 0;
	        $config['allowed_types'] = 'jpg|jpeg|png|gif';
	        $config['file_name'] = $_FILES['img']['name'];

	        $this->load->library('upload',$config);
	        $this->upload->initialize($config);

	        if($this->upload->do_upload('img')){
	            $uploadData = $this->upload->data();
	            $picture = $uploadData['file_name'];               	           
	        }
			else{
				$picture = '';
			}
		}

		$fid = $name.rand(0,1000);

		$sql = "INSERT INTO foods (name, taste, price, img, rid, fid)
						   VALUES ('$name', '$taste', '$price', '$picture', '$rid', '$fid')";
		if($this->db->query($sql)){				   	
			return true;
		}
		else 
			return false;
	}

	function getFoodDetails($resId){
		$get = "SELECT f.name as fname, f.taste, f.price, f.img, f.rating, f.fid, r.name, r.type, r.address FROM foods f JOIN restaurent r ON f.rid = r.rid WHERE f.rid = '$resId'";
		$query = $this->db->query($get);
	   	return $query->result_array();
	}

	function getOrders($rid){
		$get = "SELECT f.*, o.name as uname, o.contact, o.address, o.oid, o.quan FROM foods f JOIN order_details o ON o.fid = f.fid WHERE o.rid ='$rid' AND o.state =1";
		$query = $this->db->query($get);
	   	return $query->result_array();
	}	

	function clearOrder($oid){
		$sql = "UPDATE order_details SET state='0' WHERE oid = '$oid'";
		$this->db->query($sql);
	}

	function updateFood(){
		$name = $this->input->post('name');
		$taste = $this->input->post('taste');
		$price = $this->input->post('price');		
		$picture = $this->input->post('imgid');
		$fid = $this->input->post('fid');

		if(!empty($_FILES['img']['name'])){
			$config['upload_path'] ='img/food/';
			$config['max_size'] = 0;
	        $config['allowed_types'] = 'jpg|jpeg|png|gif';
	        $config['file_name'] = $_FILES['img']['name'];

	        $this->load->library('upload',$config);
	        $this->upload->initialize($config);

	        if($this->upload->do_upload('img')){
	            $uploadData = $this->upload->data();
	            $picture = $uploadData['file_name'];               	           
	        }
			else{
				$picture = '';
			}
		}
		$sql ="UPDATE foods SET name ='$name', taste = '$taste', price= '$price', img='$picture' WHERE fid ='$fid'";
		$this->db->query($sql);
	}
}
