<?php
class Search_Model extends CI_Model {

	function findFood(){
		$taste = explode(" ",$this->input->post('taste'));
		$taste1 = $this->input->post('taste');
		$type = $this->input->post('type');
		$price = $this->input->post('price');
		$lowp = $price-20;
		$highp = $price+20;
		$place = $this->input->post('place');
		$review = $this->input->post('review');

		$this->session->set_userdata('taste', $taste1);
		$this->session->set_userdata('type', $type);
		$this->session->set_userdata('price', $price);
		$this->session->set_userdata('review', $review);
		$this->session->set_userdata('place', $place);

		if(sizeof($taste)>1){
		$get = "SELECT f.*, r.name as rname,r.type, r.address FROM foods f JOIN restaurent r ON r.rid = f.rid WHERE f.taste LIKE '%$taste[1]%' AND (f.price BETWEEN '$lowp' AND '$highp' OR f.price <= '$price') AND r.type LIKE '%$type%' AND r.address LIKE '%mirpur%' AND f.rating >= '$review' ORDER BY f.price ASC";
		}else{
			$get = "SELECT f.*, r.name as rname,r.type, r.address FROM foods f JOIN restaurent r ON r.rid = f.rid WHERE f.taste LIKE '%$taste1%' AND (f.price BETWEEN '$lowp' AND '$highp' OR f.price <= '$price') AND r.type LIKE '%$type%' AND r.address LIKE '%mirpur%' AND f.rating >= '$review' ORDER BY f.price ASC";
		}
		$query = $this->db->query($get);
		return $query->result_array();
	}

	function getCustomerDetails($username){
		$get = "SELECT * FROM Customer WHERE username = '$username'";
		$query = $this->db->query($get);
		return $query->result_array();
	}

	function getFoodDetails($fid){
		$get = "SELECT f.name as fname, f.taste, f.price, f.img, f.rating, f.fid, f.rid, r.name, r.type, r.address FROM foods f JOIN restaurent r ON f.rid = r.rid WHERE f.fid = '$fid'";
		$query = $this->db->query($get);
		return $query->result_array();
	}

	function insertOrder(){
		$customerName = $this->input->post('name');
		$contact = $this->input->post('contact');
		$address = $this->input->post('address');
		$username = $this->session->userdata('cus_name');
		$fid = $this->input->post('fid');
		$rid = $this->input->post('rid');
		$quan = $this->input->post('quantity');

		$sql ="INSERT INTO order_details (name, contact, address, quan, username, fid, rid)
							      VALUES ('$customerName', '$contact', '$address','$quan', '$username', '$fid', '$rid')";	
		$this->session->set_userdata('cus_err',"Order Placed Successfully");
		$this->db->query($sql);							      
	}
}