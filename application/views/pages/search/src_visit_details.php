<div class="w3-row w3-center w3-padding-16">
	<h3 class="w3-text-orange"><b><?php $rname.' '.$place?></b></h3>
  <?php
       $rname = str_replace(" ", "+", $rname);
       echo $rname.'+'.$place;
  ?>
    <iframe width="80%" height="500" frameborder="0" style="border:0"
        src="https://www.google.com/maps/embed/v1/directions?origin=North+South+University&destination=<?php echo $rname.'+'.$place?>&key=AIzaSyCZfaX7In90nH2yCzwaVdvS8XMwVyD76QM" allowfullscreen class=" w3-card-4 w3-round">
    </iframe>
</div>     

<div class="w3-row w3-center">
	<a href="#" class="w3-button w3-orange w3-card-4 w3-margin-bottom">CONFIRM GOING</a>
</div>

<script>
function w3_open() {
  document.getElementById("main").style.marginLeft = "300px";
  document.getElementById("miniNav").style.display = "none";
  document.getElementById("mySidebar").style.width = "25%";
  document.getElementById("mySidebar").style.display = "block";
  document.getElementById("openNav").style.display = 'none';
}
function w3_close() {
  document.getElementById("main").style.marginLeft = "70px";
  document.getElementById("miniNav").style.display = "block";
  document.getElementById("mySidebar").style.display = "none";
  document.getElementById("openNav").style.display = "inline-block";
}
</script>
 	