  <div class="w3-padding-16" style="margin-top: 15px">
    <div class="w3-row">
      <div class="w3-center" style="margin:0px 30% 0px 30%">
          <h3 class="w3-text-orange"><b>CONFIRM DELIVERY INFORMATIONS</b></h3>
          <form action="<?php echo base_url()?>search/confirm-order" method="post" >
            <?php foreach ($customer as $c) {?>          
       		  <input type="text" name="name" class="w3-input w3-border" placeholder="Enter your name..." value="<?php echo $c['fname'].' '.$c['lname']?>">
            <br> 
            <input type="number" name="contact" class="w3-input w3-border" placeholder="Enter your number..." value="<?php echo '0'.$c['contact']?>">      
            <br>
            <input type="text" name="address" class="w3-input w3-border" placeholder="Enter your delivery Address..." value="<?php echo $c['address']?>">   
            <br> 
            <?php } ?>           
            <div class="w3-row">
            	<h3 class="w3-text-orange"><b>CONFIRM FOOD INFORMATIONS</b></h3>
            	<div class="w3-col l2 w3-padding-16 w3-center">
            		<label><b>Quantity</b></label>
            		<input type="number" name="quantity"  class="w3-input w3-center" id='quantity' placeholder="Enter your quantity..." value="<?php echo $quan?>" onchange="calcuTotal()" style="max-width: 70%;margin: 0px auto"> 
            	</div>

              <?php foreach ($food as $r) {?>                
              	<div class="w3-col l5 w3-padding" style="text-align: left;">
              		<img src="<?php echo base_url().'img/food/'.$r['img']?>" style="max-width: 100%" class="w3-card-2 w3-round">
              	</div>
              	<div class="w3-col l5 w3-padding-16" style="text-align: left;">
              		<span><b>Name: </b><?php echo $r['fname']?></span><br>
              		<span><b>Restaurent: </b><?php echo $r['name']?></span><br>
              		<span><b>Total Quantity: </b><span id='quan'> <?php echo $quan?></span></span><br>
              		<span><b>Unit Price: </b> <span id='price'><?php echo $r['price']?></span>/- Tk.</span><br>
              		<span><b>Total Price: </b><span id='total'>450</span>/- Tk.</span><br>
              	</div>
                <input type="text" name="fid" value="<?php echo $r['fid']?>" class="w3-hide">
                <input type="text" name="rid" value="<?php echo $r['rid']?>" class="w3-hide">
              <?php }?>
            </div>                  
            <hr>
            <button class="w3-btn w3-orange w3-round w3-text-white"><b>CONFIRM ORDER</b></button>
          </form>
      </div>
    </div>
  </div>
</div>
<script>
function w3_open() {
  document.getElementById("main").style.marginLeft = "300px";
  document.getElementById("miniNav").style.display = "none";
  document.getElementById("mySidebar").style.width = "25%";
  document.getElementById("mySidebar").style.display = "block";
  document.getElementById("openNav").style.display = 'none';
}
function w3_close() {
  document.getElementById("main").style.marginLeft = "70px";
  document.getElementById("miniNav").style.display = "block";
  document.getElementById("mySidebar").style.display = "none";
  document.getElementById("openNav").style.display = "inline-block";
}
</script>
<br>
