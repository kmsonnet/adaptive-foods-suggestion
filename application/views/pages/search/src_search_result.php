

  <div class="w3-padding-16">
    <div class="w3-row w3-padding">

      <div class="w3-center w3-col l4 w3-padding w3-border-right">
         <h3 class="w3-text-orange"><b>SEARCH AGAIN !</b></h3>
          <form action="<?php echo base_url()?>search/result" method="post">                  
            <select class="w3-select w3-border" name="taste">             
              <option selected><?php echo $this->session->userdata('taste')?></option>
              <?php foreach ($taste as $t) {?>    
                <option value="<?php echo $t['taste']?>"><?php echo $t['taste']?></option>
              <?php }?>
            </select>
            <br>
            <br>
            <select class="w3-select w3-border" name="type">
              <option selected><?php echo $this->session->userdata('type')?></option>
               <?php foreach ($type as $ty) {?>    
                <option value="<?php echo $ty['type']?>"><?php echo $ty['type']?></option>
              <?php }?>
            </select>
            <br>
            <br>
            <input type="number" name="price" class="w3-input w3-border" placeholder="Enter price per person ..." value="<?php echo $this->session->userdata('price')?>">           
            <br>            
            <input type="text" name="place" class="w3-input w3-border" placeholder="Enter desired area ..." value="<?php echo $this->session->userdata('place')?>">  
            <br>            
            <input type="text" name="review" placeholder="Enter Minimum Review ..." class="w3-input w3-border" value="<?php echo $this->session->userdata('review')?>">
            <hr>
            <button class="w3-btn w3-orange w3-round w3-text-white"><b>SEARCH FOODS</b></button>
          </form>
      </div>

     
      <div class="w3-center w3-col l8">
          <div class="w3-row-padding w3-center">
             <?php foreach ($result as $r) {?>       
                <div class="w3-col l4 w3-padding w3-round w3-tiny w3-animate-zoom">
                  <div class="w3-card-2 w3-padding">
                     <img src="<?php echo base_url().'img/food/'.$r['img']?>" style="width: 150px;height:100px" class="w3-card-2">
                  <br>
                  <div style="text-align: left">                            
                        <span><b>Name: </b><?php echo $r['name']?></span><br>
                        <span><b>Price: </b><?php echo $r['price']?> Tk/-</span><br>
                        <span><b>Restaurent: </b><?php echo $r['rname']?></span><br>
                        <span><b>Taste: </b><?php echo $r['taste']?></span><br>                  
                        <span><b>Rating: </b><?php echo $r['rating']?></span><br>                  
                        <span><b>Address: </b><?php echo $r['address']?></span><br>                  
                  </div>    
                  </div>                  
                  <br>   
                  <div class="w3-center">
                     <button class=" w3-left w3-button w3-green w3-small w3-round w3-margin-left" onclick="document.getElementById('<?php echo $r['fid']?>').style.display='block'">Order Food</button>
                    <form action="<?php echo base_url()?>search/visit-details" method="post" class="w3-left w3-margin-left">
                        <input type="text" name="visitadd" class="w3-hide" value="<?php echo $r['address'] ?>">
                        <input type="text" name="visitrname" class="w3-hide" value="<?php echo $r['rname'] ?>">
                        <button class="w3-button w3-red w3-small w3-round">Visit</button>
                    </form>
                  </div>                      
              
            </div>

             <div id="<?php echo $r['fid']?>" class="w3-modal w3-animate-zoom">
               <div class="w3-modal-content  w3-round-xxlarge" style="width: 500px">
                  <header class="w3-container w3-teal"> 
                    <span onclick="document.getElementById('<?php echo $r['fid']?>').style.display='none'" 
                    class="w3-button w3-display-topright">&times;</span>
                    <h2 class="w3-center">Food Details</h2>
                  </header>
                  <div class="w3-container w3-padding-32 w3-center w3-light-grey">
                      <div class="w3-row w3-padding">
                        <img src="<?php echo base_url().'img/food/'.$r['img']?>" style="width: 250px;height:200px" class="w3-card-2">
                          <br>
                          <div style="text-align: left">                            
                                <span><b>Name: </b><?php echo $r['name']?></span><br>
                                <span><b>Price: </b><?php echo $r['price']?> Tk/-</span><br>
                                <span><b>Restaurent: </b><?php echo $r['rname']?></span><br>
                                <span><b>Taste: </b><?php echo $r['taste']?></span><br>                  
                                <span><b>Rating: </b><?php echo $r['rating']?></span><br>                  
                                <span><b>Address: </b><?php echo $r['address']?></span><br> 

                          </div>     
                          <div class="w3-center">
                            <form action="<?php echo base_url()?>/search/order-details" method="post">
                                <input type="number" name="quantity" class="w3-input w3-border" placeholder="Enter Quantity ...">
                                  <br>
                                <input type="text" name="fid" class="w3-hide" value="<?php echo $r['fid']?>">
                                  <br>
                                <button class="w3-button w3-orange">PLACE ORDER </button>
                            </form>
                             
                          </div>
                      </div>            
                  </div>
                  <footer class="w3-container w3-teal w3-center w3-padding-16">            
                  </footer>
                </div>
              </div>

            <?php }?>  
          </div>          
      </div>

    </div>
  </div>
</div>
<script>
function w3_open() {
  document.getElementById("main").style.marginLeft = "300px";
  document.getElementById("miniNav").style.display = "none";
  document.getElementById("mySidebar").style.width = "25%";
  document.getElementById("mySidebar").style.display = "block";
  document.getElementById("openNav").style.display = 'none';
}
function w3_close() {
  document.getElementById("main").style.marginLeft = "70px";
  document.getElementById("miniNav").style.display = "block";
  document.getElementById("mySidebar").style.display = "none";
  document.getElementById("openNav").style.display = "inline-block";
}
</script>
