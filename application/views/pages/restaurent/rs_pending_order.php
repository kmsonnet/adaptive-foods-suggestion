
		<div class="w3-row w3-center">            
			<h2 class="w3-text-red"><b>PENDING ORDERS</b></h2>
			<?php foreach ($orders as $o) {?>				
            <div class="w3-col l3 w3-padding w3-round w3-tiny w3-animate-zoom">
            	<div class="w3-card-2 w3-padding w3-light-grey">
            		  <img src="<?php echo base_url().'img/food/'.$o['img']?>" style="width: 200px;height: 150px" class="w3-card-2 w3-round">
		              <br>
		              <div style="text-align: left">
		                    <span><b>Name: </b> <?php echo $o['name']?></span><br>
		                    <span><b>Price: </b><?php echo $o['price']?> Tk/-</span><br>		                    
		                    <span><b>Quantity: </b><?php echo $o['quan']?> </span><br>		                    
		                    <span><b>Customer's Name: </b><?php echo $o['uname']?></span><br>
		                    <span><b>Contact: </b><?php echo $o['contact']?></span><br>
		                    <span><b>Address: </b><?php echo $o['address']?></span><br>

		              </div>        
		              <br>
		              <a href="<?php echo base_url().'restaurent/clear-order/'.$o['oid']?>" class="w3-button w3-green w3-tiny w3-round">DELIVERED</a>
            	</div>              
            </div>
            <?php }?>
        </div>          

<script>
function w3_open() {
  document.getElementById("main").style.marginLeft = "300px";
  document.getElementById("miniNav").style.display = "none";
  document.getElementById("mySidebar").style.width = "25%";
  document.getElementById("mySidebar").style.display = "block";
  document.getElementById("openNav").style.display = 'none';
}
function w3_close() {
  document.getElementById("main").style.marginLeft = "70px";
  document.getElementById("miniNav").style.display = "block";
  document.getElementById("mySidebar").style.display = "none";
  document.getElementById("openNav").style.display = "inline-block";
}
</script>
