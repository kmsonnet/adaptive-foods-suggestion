
  <div class="w3-padding">   
    <div class="w3-row">
      <div class="w3-light-grey w3-padding foodList w3-round w3-border w3-card-2" style="padding-right: 30px; height: 550px;width:550px;overflow: auto;margin:0px auto">
          <h3 class="w3-center w3-border-bottom w3-text-orange"><b>AVAILABLE FOODS</b></h3>  
          <?php foreach ($foods as $f) {?>          
          <div class="w3-row">
              <img src="<?php echo base_url().'img/food/'.$f['img']?>" style="width:180px;height:150px " class="w3-left w3-card-2 w3-border w3-round">
              <span class="w3-left w3-margin-left"><b>Name: </b> <?php echo $f['fname']?></span><br>
              <span class="w3-left w3-margin-left"><b>Price: </b><?php echo $f['price']?> Tk.</span>
              <br>
              <button class="w3-margin-left w3-button w3-teal w3-small" onclick="document.getElementById('<?php echo $f['fid']?>').style.display='block'">Show Details </button>
          </div>
          <hr>

          <div id="<?php echo $f['fid']?>" class="w3-modal ">
           <div class="w3-modal-content w3-round-xxlarge" style="width: 500px">
              <header class="w3-container w3-teal"> 
                <span onclick="document.getElementById('<?php echo $f['fid']?>').style.display='none'" 
                class="w3-button w3-display-topright">&times;</span>
                <h2 class="w3-center">Food Details</h2>
              </header>
              <div class="w3-container w3-padding-32 w3-center w3-light-grey">
                  <div class="w3-row w3-padding">
                    <img src="<?php echo base_url().'img/food/'.$f['img']?>" style="max-width: 50%" class="w3-card-2">
                    <div style="text-align: left;">              
                      <form action="<?php echo base_url()?>/restaurent/update-food" method="post" enctype="multipart/form-data">          
                        <p><b>Food Image</b> <input type="file" name="img" class="w3-input w3-border"></p>
                        <p><b>Name:</b> <input type="text" name="name" value="<?php echo $f['fname']?>" class="w3-input w3-border"></p>                        
                        <p><b>Price:</b> <input type="text" name="price" value="<?php echo $f['price']?>" class="w3-input w3-border"></p>
                        <p><b>Taste:</b> <input type="text" name="taste" value="<?php echo $f['taste']?>" class="w3-input w3-border"> </p>
                        <input type="text" name="fid" value="<?php echo $f['fid']?>" class="w3-hide">
                        <input type="text" name="imgid" value="<?php echo $f['img']?>" class="w3-hide">
                        <div class="w3-center">
                          <button class="w3-button w3-green">Updated</button>  
                        </div>
                        
                      </form>      
                    </div>                                               
                  </div>            
              </div>
              <footer class="w3-container w3-teal w3-center w3-padding-16">            
              </footer>
            </div>
         </div>
        <?php }?>
    </div>
  </div>
</div>
  <br>
<script>
function w3_open() {
  document.getElementById("main").style.marginLeft = "300px";
  document.getElementById("miniNav").style.display = "none";
  document.getElementById("mySidebar").style.width = "25%";
  document.getElementById("mySidebar").style.display = "block";
  document.getElementById("openNav").style.display = 'none';
}
function w3_close() {
  document.getElementById("main").style.marginLeft = "70px";
  document.getElementById("miniNav").style.display = "block";
  document.getElementById("mySidebar").style.display = "none";
  document.getElementById("openNav").style.display = "inline-block";
}
</script>
