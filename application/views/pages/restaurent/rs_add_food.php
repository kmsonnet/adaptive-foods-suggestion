  <?php 
    $msg = $this->session->userdata('res_err');
  ?>  
  <?php if($msg !=''){?>
    <div class="w3-panel w3-center w3-green w3-display-container w3-animate-zoom" style="max-width: 500px;margin: 30px auto">
      <span onclick="this.parentElement.style.display='none'"
      class="w3-button w3-green w3-large w3-display-topright">&times;</span>
      <h3><?php echo $msg?></h3>
    </div>
  <?php }?>

  <div class="w3-padding-32" style="margin-bottom:7px">
    <div class="w3-row w3-row-padding">
      <div class="w3-center" style="width: 500px; margin: 0px auto">
          <h3 class="w3-text-orange"><b>ADD NEW FOOD TO MENU</b></h3>
          <form action="<?php echo base_url()?>restaurent/store-food" method="post" enctype="multipart/form-data">
            <input type="text" name="name" class="w3-input w3-border" placeholder="Enter Food Name ...">
            <br>    
            <input type="text" name="taste" class="w3-input w3-border" placeholder="Enter Food Taste ...">
            <br>            
            <input type="text" name="price" class="w3-input w3-border" placeholder="Enter Food Price ...">               
            <br>                   
            <input type="file" name="img" class="w3-input w3-border">        
            <br>
            <button class="w3-btn w3-orange w3-round "><b>ADD FOOD</b></button>
          </form>
      </div>
    </div>
  </div>
  <script>
    function w3_open() {
      document.getElementById("main").style.marginLeft = "300px";
      document.getElementById("miniNav").style.display = "none";
      document.getElementById("mySidebar").style.width = "25%";
      document.getElementById("mySidebar").style.display = "block";
      document.getElementById("openNav").style.display = 'none';
    }
    function w3_close() {
      document.getElementById("main").style.marginLeft = "70px";
      document.getElementById("miniNav").style.display = "block";
      document.getElementById("mySidebar").style.display = "none";
      document.getElementById("openNav").style.display = "inline-block";
    }
  </script>
<br>
<br>
<br>