  <div class="w3-padding">   
    <div class="w3-row" >
      <div class="w3-light-grey w3-padding foodList w3-round w3-border w3-card-2" style="padding-right: 30px; height: 550px;width:550px;overflow: auto;margin:0px auto">
          <h3 class="w3-center w3-border-bottom w3-text-orange"><b>HISTORY OF FOOD YOU TASTED</b></h3>              
          <?php foreach ($history as $h){?>            
            <div class="w3-row">
                <img src="<?php echo base_url().'img/food/'.$h['img']?>" style="max-width:200px" class="w3-left w3-card-2 w3-border w3-round">
                <span class="w3-left w3-margin-left"><b>Name: </b> <?php echo $h['name']?></span><br>
                <span class="w3-left w3-margin-left"><b>Restaurent: </b> <?php echo $h['rname']?></span><br>                                
                <span class="w3-left w3-margin-left"><b>Price: </b> <?php echo $h['price']?> /-Tk</span>    <br>                            
                <button class="w3-margin-left w3-button w3-teal w3-small" onclick="document.getElementById('<?php echo $h['fid']?>').style.display='block'" >Show Details </button>
            </div>
            <hr>   
             <div id="<?php echo $h['fid']?>" class="w3-modal ">
               <div class="w3-modal-content  w3-round-xxlarge" style="width: 500px">
                  <header class="w3-container w3-teal"> 
                    <span onclick="document.getElementById('<?php echo $h['fid']?>').style.display='none'" 
                    class="w3-button w3-display-topright">&times;</span>
                    <h2 class="w3-center">Food Details</h2>
                  </header>
                  <div class="w3-container w3-padding-32 w3-center w3-light-grey">
                      <img src="<?php echo base_url().'img/food/'.$h['img']?>" style="max-width: 60%">
                      <div class="w3-row w3-padding">                        
                        <p><b>Food Name:</b> <?php echo $h['name']?></p>
                        <p><b>Price: </b> <?php echo $h['price']?> /-Tk</p>
                        <p><b>Taste:</b> <?php echo $h['taste']?>
                          <!--  <select>
                            <option selected="">Change</option>
                            <option>Less Spicy</option>
                            <option>Spicy</option>
                            <option>Very Spicy</option>
                           </select> -->
                        </p>
                        <p><b>Restaurent:</b> Coffee Time</p>                        
                      </div>            
                      <!-- <button class="w3-button w3-green">Updated</button> -->
                  </div>
                  <footer class="w3-container w3-teal w3-center w3-padding-16">            
                  </footer>
                </div>
             </div>   
          <?php }?>    
      </div>
    </div>
  </div>
</div>
<script>
function w3_open() {
  document.getElementById("main").style.marginLeft = "300px";
  document.getElementById("miniNav").style.display = "none";
  document.getElementById("mySidebar").style.width = "25%";
  document.getElementById("mySidebar").style.display = "block";
  document.getElementById("openNav").style.display = 'none';
}
function w3_close() {
  document.getElementById("main").style.marginLeft = "70px";
  document.getElementById("miniNav").style.display = "block";
  document.getElementById("mySidebar").style.display = "none";
  document.getElementById("openNav").style.display = "inline-block";
}
</script>
