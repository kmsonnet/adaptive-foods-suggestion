  <div class="w3-padding">   
    <div class="w3-row">
      <div class="w3-light-grey w3-padding foodList w3-round w3-border w3-card-2" style="padding-right: 30px; max-height: 550px;width:550px;overflow: auto;margin:0px auto">
          <h3 class="w3-center w3-border-bottom w3-text-orange"><b>HISTORY OF FOOD YOU REVIEWED</b></h3>              
          <div class="w3-row">
              <img src="<?php echo base_url()?>img/i1.jpg" style="max-width:200px" class="w3-left w3-card-2 w3-border w3-round">
              <span class="w3-left w3-margin-left"><b>Name: </b> Food Name</span><br>
              <span class="w3-left w3-margin-left"><b>Restaurent: </b> Restaurent Name</span><br>
              <span class="w3-left w3-margin-left"><b>Date:</b>04-Sep-2017</span>
              <br>
              <button class="w3-margin-left w3-button w3-teal w3-small">Show Details </button>
          </div>
          <hr>

          <div class="w3-row">
              <img src="<?php echo base_url()?>img/i1.jpg" style="max-width:200px" class="w3-left w3-card-2 w3-border w3-round">
              <span class="w3-left w3-margin-left"><b>Name: </b> Food Name</span><br>
              <span class="w3-left w3-margin-left"><b>Restaurent: </b> Restaurent Name</span><br>
              <span class="w3-left w3-margin-left"><b>Date:</b>04-Sep-2017</span>
              <br>
              <button class="w3-margin-left w3-button w3-teal w3-small">Show Details </button>
          </div>
          <hr>

          <div class="w3-row">
              <img src="<?php echo base_url()?>img/i1.jpg" style="max-width:200px" class="w3-left w3-card-2 w3-border w3-round">
              <span class="w3-left w3-margin-left"><b>Name: </b> Food Name</span><br>
              <span class="w3-left w3-margin-left"><b>Restaurent: </b> Restaurent Name</span><br>
              <span class="w3-left w3-margin-left"><b>Date:</b>04-Sep-2017</span>
              <br>
              <button class="w3-margin-left w3-button w3-teal w3-small">Show Details </button>
          </div>
          <hr>

          <div class="w3-row">
              <img src="<?php echo base_url()?>img/i1.jpg" style="max-width:200px" class="w3-left w3-card-2 w3-border w3-round">
              <span class="w3-left w3-margin-left"><b>Name: </b> Food Name</span><br>
              <span class="w3-left w3-margin-left"><b>Restaurent: </b> Restaurent Name</span><br>
              <span class="w3-left w3-margin-left"><b>Date:</b>04-Sep-2017</span>
              <br>
              <button class="w3-margin-left w3-button w3-teal w3-small">Show Details </button>
          </div>
          <hr>

          <div class="w3-row">
              <img src="<?php echo base_url()?>img/i1.jpg" style="max-width:200px" class="w3-left w3-card-2 w3-border w3-round">
              <span class="w3-left w3-margin-left"><b>Name: </b> Food Name</span><br>
              <span class="w3-left w3-margin-left"><b>Restaurent: </b> Restaurent Name</span><br>
              <span class="w3-left w3-margin-left"><b>Date:</b>04-Sep-2017</span>
              <br>
              <button class="w3-margin-left w3-button w3-teal w3-small">Show Details </button>
          </div>
          <hr>
          
      </div>

      <div id="foodDetails" class="w3-modal ">
       <div class="w3-modal-content  w3-round-xxlarge" style="width: 500px">
          <header class="w3-container w3-teal"> 
            <span onclick="document.getElementById('foodDetails').style.display='none'" 
            class="w3-button w3-display-topright">&times;</span>
            <h2 class="w3-center">Food Details</h2>
          </header>
          <div class="w3-container w3-padding-32 w3-center w3-light-grey">
              <div class="w3-row w3-padding">
                <img src="img/i1.jpg" style="max-width: 60%">
                <p><b>Food Name:</b> Some Name</p>
                <p><b>Price:</b> 179 Tk/-</p>
                <p><b>Taste:</b> Less Spicy
                   <select>
                    <option selected="">Change</option>
                    <option>Less Spicy</option>
                    <option>Spicy</option>
                    <option>Very Spicy</option>
                   </select>
                </p>
                <p><b>Restaurent:</b> Coffee Time</p>
                <button class="w3-button w3-green">Updated</button>
              </div>            
          </div>
          <footer class="w3-container w3-teal w3-center w3-padding-16">            
          </footer>
        </div>
     </div>
  
    </div>
  </div>
</div>
<script>
function w3_open() {
  document.getElementById("main").style.marginLeft = "300px";
  document.getElementById("miniNav").style.display = "none";
  document.getElementById("mySidebar").style.width = "25%";
  document.getElementById("mySidebar").style.display = "block";
  document.getElementById("openNav").style.display = 'none';
}
function w3_close() {
  document.getElementById("main").style.marginLeft = "70px";
  document.getElementById("miniNav").style.display = "block";
  document.getElementById("mySidebar").style.display = "none";
  document.getElementById("openNav").style.display = "inline-block";
}
</script>
