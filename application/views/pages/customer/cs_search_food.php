﻿  <?php 
    $msg = $this->session->userdata('cus_err');
  ?>  
  <?php if($msg !=''){?>
    <div class="w3-panel w3-center w3-green w3-display-container w3-animate-zoom" style="max-width: 500px;margin: 30px auto">
      <span onclick="this.parentElement.style.display='none'"
      class="w3-button w3-green w3-large w3-display-topright">&times;</span>
      <h3><?php echo $msg?></h3>
    </div>
  <?php }?>

  <div class="w3-padding-16">
    <div class="w3-row">
      <div class="w3-center" style="margin:0px 30% 0px 30%">
          <h3 class="w3-text-orange"><b>SEARCH YOUR PREFERED FOODS</b></h3>
          <form action="<?php echo base_url()?>search/result" method="post">                  
            <select class="w3-select w3-border" name="taste">             
              <option disabled selected>Choose Taste</option>
              <?php foreach ($taste as $t) {?>    
                <option value="<?php echo $t['taste']?>"><?php echo $t['taste']?></option>
              <?php }?>
            </select>
            <br>
            <br>
            <select class="w3-select w3-border" name="type">
              <option disabled selected>Choose Environment</option>
               <?php foreach ($type as $ty) {?>    
                <option value="<?php echo $ty['type']?>"><?php echo $ty['type']?></option>
              <?php }?>
            </select>
            <br>
            <br>
            <input type="number" name="price" class="w3-input w3-border" placeholder="Enter price per person ...">           
            <br>            
            <input type="text" name="place" class="w3-input w3-border" placeholder="Enter desired area ...">  
            <br>            
            <input type="text" name="review" placeholder="Enter Minimum Review ..." class="w3-input w3-border">
            <hr>
            <button class="w3-btn w3-orange w3-round w3-text-white"><b>SEARCH FOODS</b></button>
          </form>
      </div>
    </div>
  </div>
</div>
<script>
function w3_open() {
  document.getElementById("main").style.marginLeft = "300px";
  document.getElementById("miniNav").style.display = "none";
  document.getElementById("mySidebar").style.width = "25%";
  document.getElementById("mySidebar").style.display = "block";
  document.getElementById("openNav").style.display = 'none';
}
function w3_close() {
  document.getElementById("main").style.marginLeft = "70px";
  document.getElementById("miniNav").style.display = "block";
  document.getElementById("mySidebar").style.display = "none";
  document.getElementById("openNav").style.display = "inline-block";
}
</script>
<br>
<br>