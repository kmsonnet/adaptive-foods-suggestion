<!DOCTYPE html>
<html>
<head>
	<title>AFS | Home</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/w3.css">
	<link rel="stylesheet" type="text/css" href="css/custom.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="js/custom.js"></script>

	<script src="carouselengine/jquery.js"></script>
    <script src="carouselengine/amazingcarousel.js"></script>
    <link rel="stylesheet" type="text/css" href="carouselengine/initcarousel-1.css">
    <script src="carouselengine/initcarousel-1.js"></script>

    <script src="carouselengine2/jquery.js"></script>
    <script src="carouselengine2/amazingcarousel.js"></script>
    <link rel="stylesheet" type="text/css" href="carouselengine2/initcarousel-2.css">
    <script src="carouselengine2/initcarousel-2.js"></script>

    <script src="carouselengine3/jquery.js"></script>
    <script src="carouselengine3/amazingcarousel.js"></script>
    <link rel="stylesheet" type="text/css" href="carouselengine3/initcarousel-3.css">
    <script src="carouselengine3/initcarousel-3.js"></script>

</head>

<body>

	<!-- Navbar (sit on top) -->
	<div class="w3-top">
	  <div class="w3-bar w3-black" id="navBar">
	    <a class="w3-bar-item w3-button w3-hover-black w3-hide-medium w3-hide-large w3-right w3-margin-top" href="javascript:void(0);" onclick="toggleFunction()" title="Toggle Navigation Menu">
	      <i class="fa fa-bars"></i>
	    </a>

	    <a href="javascript:void(0)" class="w3-bar-item"><img src="img/logo.png" width="55%">
	    </a>
	    <?php 
			$resName = $this->session->userdata('res_name');			
			$cusName = $this->session->userdata('cus_name');			
		?>
		<?php if($resName == '' && $cusName == ''){?>
		    <a href="javascript:void(0)" class="w3-bar-item w3-button w3-hide-small w3-hover-green w3-right" onclick="document.getElementById('restaurentOwner').style.display='block'" style="height:75px;padding-top:25px;"><i class="fa fa-flask"></i> Own a Restaurent?</a>
		    <a href="javascript:void(0)" class="w3-bar-item w3-button w3-hide-small w3-hover-green w3-right" style="height:75px;padding-top:25px;" onclick="document.getElementById('foodLover').style.display='block'"><i class="fa fa-glass"></i> Food Lover?</a>

	    <?php } else if($resName != ''){?>
	   		<a href="restaurent/logout" class="w3-bar-item w3-button w3-hide-small w3-hover-green w3-right" style="height:75px;padding-top:25px;"><i class="fa fa-flask"></i> LOGOUT</a>
		    <a href="restaurent/add-food" class="w3-bar-item w3-button w3-hide-small w3-hover-green w3-right" style="height:75px;padding-top:25px;text-transform: uppercase;"><i class="fa fa-glass"></i> <?php echo $resName?></a>

		<?php } else if($cusName != ''){?>	    
	   		<a href="restaurent/logout" class="w3-bar-item w3-button w3-hide-small w3-hover-green w3-right" style="height:75px;padding-top:25px;"><i class="fa fa-flask"></i> LOGOUT</a>
		    <a href="customer/search-food" class="w3-bar-item w3-button w3-hide-small w3-hover-green w3-right" style="height:75px;padding-top:25px;text-transform: uppercase;"><i class="fa fa-glass"></i> <?php echo $cusName?></a>
		<?php }?>

	    <a href="javascript:void(0)" class="w3-bar-item w3-white w3-button w3-hide-small  w3-hover-green w3-right" style="height:75px;padding-top:25px;"><i class="fa fa-home"></i> HOME</a>

	  </div>

	  <!-- Navbar on small screens -->
	  <div id="navSmall" class="w3-bar-block w3-white w3-hide w3-hide-large w3-hide-medium ">
	    <a href="javascript:void(0)" class="w3-bar-item w3-button" onclick="toggleFunction()">HOME</a>
	    <a href="javascript:void(0)" class="w3-bar-item w3-button" onclick="toggleFunction()">Food Lover?</a>
	    <a href="javascript:void(0)" class="w3-bar-item w3-button" onclick="toggleFunction()"> Own a Restaurent?</a>    	   
	  </div>
	</div>

	<!-- Trigger/Open the Modal -->	

	<!-- The customer login -->
	<div id="foodLover" class="w3-modal ">
	 	 <div class="w3-modal-content  w3-round-xxlarge" style="width: 500px">
	      <header class="w3-container w3-teal"> 
	        <span onclick="document.getElementById('foodLover').style.display='none'" 
	        class="w3-button w3-display-topright">&times;</span>
	        <h2 class="w3-center">Login to Discover</h2>
	      </header>
	      <div class="w3-container w3-padding-32 w3-center ">
	       	<form method="post" action="customer/login">
	       		<input type="text" name="username" placeholder="Enter username ..." class="w3-input w3-border">
	       		<br>
	       		<input type="password" name="password" placeholder="Enter password ..." class="w3-input w3-border">
	       		<br>
	       		<button class="w3-btn w3-green w3-round-xlarge">LOGIN</button>
	       	</form>
	      </div>
	      <footer class="w3-container w3-teal w3-center w3-padding-16">
	         <a href="#" onclick="showAndHide('foodLoverReg', 'foodLover')"  style="text-decoration: none">Create new account?</a>
	         <br>
	        <a href="#" style="text-decoration: none">Forgot password?</a>
	      </footer>
    	</div>
 	 </div>

 	  <!-- The customers Signup -->
 	 <div id="foodLoverReg" class="w3-modal ">
	 	 <div class="w3-modal-content  w3-round-xxlarge" style="width: 550px">
	      <header class="w3-container w3-teal"> 
	        <span onclick="document.getElementById('foodLoverReg').style.display='none'" 
	        class="w3-button w3-display-topright">&times;</span>
	        <h2 class="w3-center">Join to Discover</h2>
	      </header>
	      <div class="w3-container w3-padding-32 w3-center ">
	       	<form action="customer/signup" method="post"> 	       
	       		<div class="w3-row-padding">
	       			<div class="w3-half">
	       				<input type="text" name="fname" placeholder="Enter First Name ..." class="w3-input w3-border">	       		
	       			</div>
	       			<div class="w3-half">
	       				<input type="text" name="lname" placeholder="Enter Last Name ..." class="w3-input w3-border">
	       			</div>
	       			
	       		</div>	       		
	       		<br>
	       		<div class="w3-padding">
	       			<input type="text" name="username" placeholder="Choose a username ..." class="w3-input w3-border">
		       		<br>
		       		<input type="password" name="password" placeholder="Choose a  password ..." class="w3-input w3-border">
		       		<br>
		       		<input type="password" name="password" placeholder="Confirm password ..." class="w3-input w3-border">	       
		       		<br>
		       		<input type="number" name="contact" placeholder="Enter Contact Number ..." class="w3-input w3-border">
		       		<br>
		       		<input type="address" name="address" placeholder="Enter Present Address ..." class="w3-input w3-border">
	       		</div>	       		
	       		<button class="w3-btn w3-green w3-round-large">JOIN</button>
	       	</form>
	      </div>
	      <footer class="w3-container w3-teal w3-center w3-padding-16">	     	       
	      </footer>
    	</div>
 	 </div>

	<!-- The restaurent login -->
 	 <div id="restaurentOwner" class="w3-modal ">
	 	 <div class="w3-modal-content  w3-round-xxlarge" style="width: 500px">
	      <header class="w3-container w3-teal"> 
	        <span onclick="document.getElementById('restaurentOwner').style.display='none'" 
	        class="w3-button w3-display-topright">&times;</span>
	        <h2 class="w3-center">Login to Expand Busssiness</h2>
	      </header>
	      <div class="w3-container w3-padding-32 w3-center ">
	      	<form method="post" action="restaurent/login">
	       		<input type="text" name="name" placeholder="Enter name ..." class="w3-input w3-border">
	       		<br>
	       		<input type="password" name="password" placeholder="Enter password ..." class="w3-input w3-border">
	       		<br>
	       		<button class="w3-btn w3-green w3-round-xlarge">LOGIN</button>
	       	</form>
	      </div>
	      <footer class="w3-container w3-teal w3-center w3-padding-16">
	        <a href="#"  onclick="showAndHide('restaurentOwnerReg', 'restaurentOwner')" style="text-decoration: none">Create new account?</a>
	        <br>	        
	      </footer>
    	</div>
 	 </div>

 	 <!-- The restaurent Signup -->
 	 <div id="restaurentOwnerReg" class="w3-modal ">
	 	 <div class="w3-modal-content  w3-round-xxlarge" style="width: 500px">
	      <header class="w3-container w3-teal"> 
	        <span onclick="document.getElementById('restaurentOwnerReg').style.display='none'" 
	        class="w3-button w3-display-topright">&times;</span>
	        <h2 class="w3-center">Join to Expand Busssiness</h2>
	      </header>
	      <div class="w3-container w3-padding-32 w3-center ">
	       	<form action="restaurent/signup" method="post">	      
	       		<input type="text" name="name" placeholder="Enter Restaurent Name ..." class="w3-input w3-border">
	       		<br>
	       		<input type="text" name="username" placeholder="Choose a username ..." class="w3-input w3-border">
	       		<br>	       	      
	       		<input type="password" name="password" placeholder="Choose a Password ..." class="w3-input w3-border">
	       		<br>
	       		<input type="password" name="conpassword" placeholder="Confirm Password ..." class="w3-input w3-border">
	       		<br>
	       		<input type="email" name="email" placeholder="Enter a Valid Email ..." class="w3-input w3-border">
	       		<br>
	       		<input type="text" name="type" placeholder="Enter Restaurent Type ..." class="w3-input w3-border">
	       		<br>
	       		<input type="text" name="address" placeholder="Enter Restaurent Address ..." class="w3-input w3-border">
	       		<br>	       			       	
	       		<button class="w3-btn w3-green w3-round">JOIN</button>
	       	</form>
	      </div>
	      <footer class="w3-container w3-teal w3-center w3-padding-16">	        
	      </footer>
    	</div>
 	 </div>
	
	
	<div class="bgimg-1">
		<br>
		<br>		
		<br>
		 <?php $msg = $this->session->userdata('res_err')?>  
		  <?php if($msg =='Invalid! Try Again'){?>
		    <div class="w3-panel w3-center w3-red w3-display-container w3-animate-zoom" style="max-width: 500px;margin: 30px auto">
		      <span onclick="this.parentElement.style.display='none'"
		      class="w3-button w3-red w3-large w3-display-topright">&times;</span>
		      <h3><?php echo $msg?></h3>
		    </div>
		  <?php }?>
		<br>
		<br>
		<br>
		 
	   	<div class="w3-white w3-round-xlarge w3-padding-64 w3-opacity-min" id="top">
	   		<h1 class="w3-inline"><b>WELCOME TO</b></h1><img src="img/logo.png" class="w3-inline" style="margin-bottom: -30px">
	   		<p id="welcomeMsg" >We welcome you to Home of Fried Chicken & Bakery. Here you can have a wide range of choices of foods with lavishly decorated interior to give you an amazing experience of foods and quality time spending place. We ensure you the best food with the shortest time possible.</p>
	   	</div>		 
	</div>


	<div class="w3-row w3-padding-8 w3-amber" >
		<h3 class="w3-margin-left"><i class="fa fa-ellipsis-v"></i> Popular Spicy Foods</h3>
	</div>
<div style="margin: 0px auto">
	<!-- Insert to your webpage where you want to display the carousel -->
<div id="amazingcarousel-container-1" class=" w3-light-grey">
    <div id="amazingcarousel-1" style="display:none;position:relative;width:100%;max-width:1200px;margin:0px auto 0px;">
        <div class="amazingcarousel-list-container">
            <ul class="amazingcarousel-list">
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
						<div class="amazingcarousel-image"><a href="images/3C782A2700000578-4152656-image-a-21_1485279904321-lightbox.jpg" title="3C782A2700000578-4152656-image-a-21_1485279904321"  class="html5lightbox" data-group="amazingcarousel-1" data-thumbnail="images/3C782A2700000578-4152656-image-a-21_1485279904321.jpg" ><img src="images/3C782A2700000578-4152656-image-a-21_1485279904321.jpg"  alt="3C782A2700000578-4152656-image-a-21_1485279904321" /></a></div>
						<div class="amazingcarousel-title">3C782A2700000578-4152656-image-a-21_1485279904321</div>
						<div class="amazingcarousel-description"></div>
						<div class="amazingcarousel-readmore"></div>   
					</div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
						<div class="amazingcarousel-image"><a href="images/170201165457-02-cnn-fastfood-roundthree-final-0018-super-169-lightbox.jpg" title="170201165457-02-cnn-fastfood-roundthree-final-0018-super-169"  class="html5lightbox" data-group="amazingcarousel-1" data-thumbnail="images/170201165457-02-cnn-fastfood-roundthree-final-0018-super-169.jpg" ><img src="images/170201165457-02-cnn-fastfood-roundthree-final-0018-super-169.jpg"  alt="170201165457-02-cnn-fastfood-roundthree-final-0018-super-169" /></a></div>
						<div class="amazingcarousel-title">170201165457-02-cnn-fastfood-roundthree-final-0018-super-169</div>
						<div class="amazingcarousel-description"></div>
						<div class="amazingcarousel-readmore"></div>  
                  </div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
						<div class="amazingcarousel-image"><a href="images/Mansa-Fast-Food-lightbox.jpg" title="Mansa-Fast-Food"  class="html5lightbox" data-group="amazingcarousel-1" data-width="960" data-height="720"  data-thumbnail="images/Mansa-Fast-Food.jpg" ><img src="images/Mansa-Fast-Food.jpg"  alt="Mansa-Fast-Food" /></a></div>
						<div class="amazingcarousel-title">Mansa-Fast-Food</div>
						<div class="amazingcarousel-description"></div>
						<div class="amazingcarousel-readmore"></div>       
            		</div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
						<div class="amazingcarousel-image"><a href="images/i1-lightbox.jpg" title="i1"  class="html5lightbox" data-group="amazingcarousel-1" data-thumbnail="images/i1.jpg" ><img src="images/i1.jpg"  alt="i1" /></a></div>
						<div class="amazingcarousel-title">i1</div>
						<div class="amazingcarousel-description"></div>
						<div class="amazingcarousel-readmore"></div>
                    </div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
						<div class="amazingcarousel-image"><a href="images/i2-lightbox.jpg" title="i2"  class="html5lightbox" data-group="amazingcarousel-1" data-thumbnail="images/i2.jpg" ><img src="images/i2.jpg"  alt="i2" /></a></div>
						<div class="amazingcarousel-title">i2</div>
						<div class="amazingcarousel-description"></div>
						<div class="amazingcarousel-readmore"></div>
					</div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
						<div class="amazingcarousel-image"><a href="images/i3-lightbox.jpg" title="i3"  class="html5lightbox" data-group="amazingcarousel-1" data-thumbnail="images/i3.jpg" ><img src="images/i3.jpg"  alt="i3" /></a></div>
						<div class="amazingcarousel-title">i3</div>
						<div class="amazingcarousel-description"></div>
						<div class="amazingcarousel-readmore"></div>      
              		</div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
						<div class="amazingcarousel-image"><a href="images/b1-lightbox.jpg" title="b1"  class="html5lightbox" data-group="amazingcarousel-1" data-thumbnail="images/b1.jpg" ><img src="images/b1.jpg"  alt="b1" /></a></div>
						<div class="amazingcarousel-title">b1</div>
						<div class="amazingcarousel-description"></div>
						<div class="amazingcarousel-readmore"></div>
                    </div>
                </li>
            </ul>
            <div class="amazingcarousel-prev"></div>
            <div class="amazingcarousel-next"></div>
        </div>
        <div class="amazingcarousel-nav"></div>       
    </div>
</div>
</div>


	<div class="w3-row w3-padding-8 w3-red" >
		<h3 class="w3-margin-left"><i class="fa fa-ellipsis-v"></i> Popular Sweet Foods</h3>
	</div>
<div style="margin:0px auto;">
    
<!-- Insert to your webpage where you want to display the carousel -->
<div id="amazingcarousel-container-2">
    <div id="amazingcarousel-2" style="display:none;position:relative;width:100%;max-width:1200px;margin:0px auto 0px;">
        <div class="amazingcarousel-list-container">
            <ul class="amazingcarousel-list">
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
						<div class="amazingcarousel-image"><a href="images/3C782A2700000578-4152656-image-a-21_1485279904321-lightbox.jpg" title="3C782A2700000578-4152656-image-a-21_1485279904321"  class="html5lightbox" data-group="amazingcarousel-2" data-thumbnail="images/3C782A2700000578-4152656-image-a-21_1485279904321.jpg" ><img src="images/3C782A2700000578-4152656-image-a-21_1485279904321.jpg"  alt="3C782A2700000578-4152656-image-a-21_1485279904321" /></a></div>
						<div class="amazingcarousel-title">3C782A2700000578-4152656-image-a-21_1485279904321</div>
						<div class="amazingcarousel-description"></div>
						<div class="amazingcarousel-readmore"></div>           
			         </div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
						<div class="amazingcarousel-image"><a href="images/170201165457-02-cnn-fastfood-roundthree-final-0018-super-169-lightbox.jpg" title="170201165457-02-cnn-fastfood-roundthree-final-0018-super-169"  class="html5lightbox" data-group="amazingcarousel-2" data-thumbnail="images/170201165457-02-cnn-fastfood-roundthree-final-0018-super-169.jpg" ><img src="images/170201165457-02-cnn-fastfood-roundthree-final-0018-super-169.jpg"  alt="170201165457-02-cnn-fastfood-roundthree-final-0018-super-169" /></a></div>
						<div class="amazingcarousel-title">170201165457-02-cnn-fastfood-roundthree-final-0018-super-169</div>
						<div class="amazingcarousel-description"></div>
						<div class="amazingcarousel-readmore"><a href="images/170201165457-02-cnn-fastfood-roundthree-final-0018-super-169-lightbox.jpg" target="__TARGET__"></a></div>                  
				   </div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
<div class="amazingcarousel-image"><a href="images/Mansa-Fast-Food-lightbox.jpg" title="Mansa-Fast-Food"  class="html5lightbox" data-group="amazingcarousel-2" data-width="960" data-height="720"  data-thumbnail="images/Mansa-Fast-Food.jpg" ><img src="images/Mansa-Fast-Food.jpg"  alt="Mansa-Fast-Food" /></a></div>
<div class="amazingcarousel-title">Mansa-Fast-Food</div>
<div class="amazingcarousel-description"></div>
<div class="amazingcarousel-readmore"></div>                    </div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
<div class="amazingcarousel-image"><a href="images/i1-lightbox.jpg" title="i1"  class="html5lightbox" data-group="amazingcarousel-2" data-thumbnail="images/i1.jpg" ><img src="images/i1.jpg"  alt="i1" /></a></div>
<div class="amazingcarousel-title">i1</div>
<div class="amazingcarousel-description"></div>
<div class="amazingcarousel-readmore"><a href="images/i1-lightbox.jpg" target="__TARGET__"></a></div>                    </div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
<div class="amazingcarousel-image"><a href="images/i2-lightbox.jpg" title="i2"  class="html5lightbox" data-group="amazingcarousel-2" data-thumbnail="images/i2.jpg" ><img src="images/i2.jpg"  alt="i2" /></a></div>
<div class="amazingcarousel-title">i2</div>
<div class="amazingcarousel-description"></div>
<div class="amazingcarousel-readmore"><a href="images/i2-lightbox.jpg" target="__TARGET__"></a></div>                    </div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
<div class="amazingcarousel-image"><a href="images/i3-lightbox.jpg" title="i3"  class="html5lightbox" data-group="amazingcarousel-2" data-thumbnail="images/i3.jpg" ><img src="images/i3.jpg"  alt="i3" /></a></div>
<div class="amazingcarousel-title">i3</div>
<div class="amazingcarousel-description"></div>
<div class="amazingcarousel-readmore"><a href="images/i3-lightbox.jpg" target="__TARGET__"></a></div>                    </div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
<div class="amazingcarousel-image"><a href="images/b1-lightbox.jpg" title="b1"  class="html5lightbox" data-group="amazingcarousel-2" data-thumbnail="images/b1.jpg" ><img src="images/b1.jpg"  alt="b1" /></a></div>
<div class="amazingcarousel-title">b1</div>
<div class="amazingcarousel-description"></div>
<div class="amazingcarousel-readmore"><a href="images/b1-lightbox.jpg" target="__TARGET__"></a></div>                    </div>
                </li>
            </ul>
            <div class="amazingcarousel-prev"></div>
            <div class="amazingcarousel-next"></div>
        </div>
        <div class="amazingcarousel-nav"></div>
        <div class="amazingcarousel-engine"><a href="http://amazingcarousel.com">JavaScript Carousel</a></div>
    </div>
</div>
<!-- End of body section HTML codes -->

</div>

	<div class="w3-row w3-padding-8 w3-green" >
		<h3 class="w3-margin-left"><i class="fa fa-ellipsis-v"></i> Popular Sour Foods</h3>
	</div>
<div style="margin:0px auto;">
    
<!-- Insert to your webpage where you want to display the carousel -->
<div id="amazingcarousel-container-3" class="w3-light-grey">
    <div id="amazingcarousel-3" style="display:none;position:relative;width:100%;max-width:1200px;margin:0px auto 0px;">
        <div class="amazingcarousel-list-container">
            <ul class="amazingcarousel-list">
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
<div class="amazingcarousel-image"><a href="images/3C782A2700000578-4152656-image-a-21_1485279904321-lightbox.jpg" title="3C782A2700000578-4152656-image-a-21_1485279904321"  class="html5lightbox" data-group="amazingcarousel-3" data-thumbnail="images/3C782A2700000578-4152656-image-a-21_1485279904321.jpg" ><img src="images/3C782A2700000578-4152656-image-a-21_1485279904321.jpg"  alt="3C782A2700000578-4152656-image-a-21_1485279904321" /></a></div>
<div class="amazingcarousel-title">3C782A2700000578-4152656-image-a-21_1485279904321</div>
<div class="amazingcarousel-description"></div>
<div class="amazingcarousel-readmore"><a href="images/3C782A2700000578-4152656-image-a-21_1485279904321-lightbox.jpg" target="__TARGET__"></a></div>                    </div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
<div class="amazingcarousel-image"><a href="images/170201165457-02-cnn-fastfood-roundthree-final-0018-super-169-lightbox.jpg" title="170201165457-02-cnn-fastfood-roundthree-final-0018-super-169"  class="html5lightbox" data-group="amazingcarousel-3" data-thumbnail="images/170201165457-02-cnn-fastfood-roundthree-final-0018-super-169.jpg" ><img src="images/170201165457-02-cnn-fastfood-roundthree-final-0018-super-169.jpg"  alt="170201165457-02-cnn-fastfood-roundthree-final-0018-super-169" /></a></div>
<div class="amazingcarousel-title">170201165457-02-cnn-fastfood-roundthree-final-0018-super-169</div>
<div class="amazingcarousel-description"></div>
<div class="amazingcarousel-readmore"><a href="images/170201165457-02-cnn-fastfood-roundthree-final-0018-super-169-lightbox.jpg" target="__TARGET__"></a></div>                    </div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
<div class="amazingcarousel-image"><a href="images/Mansa-Fast-Food-lightbox.jpg" title="Mansa-Fast-Food"  class="html5lightbox" data-group="amazingcarousel-3" data-width="960" data-height="720"  data-thumbnail="images/Mansa-Fast-Food.jpg" ><img src="images/Mansa-Fast-Food.jpg"  alt="Mansa-Fast-Food" /></a></div>
<div class="amazingcarousel-title">Mansa-Fast-Food</div>
<div class="amazingcarousel-description"></div>
<div class="amazingcarousel-readmore"><a href="images/Mansa-Fast-Food-lightbox.jpg" target="__TARGET__"></a></div>                    </div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
<div class="amazingcarousel-image"><a href="images/i1-lightbox.jpg" title="i1"  class="html5lightbox" data-group="amazingcarousel-3" data-thumbnail="images/i1.jpg" ><img src="images/i1.jpg"  alt="i1" /></a></div>
<div class="amazingcarousel-title">i1</div>
<div class="amazingcarousel-description"></div>
<div class="amazingcarousel-readmore"><a href="images/i1-lightbox.jpg" target="__TARGET__"></a></div>                    </div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
<div class="amazingcarousel-image"><a href="images/i2-lightbox.jpg" title="i2"  class="html5lightbox" data-group="amazingcarousel-3" data-thumbnail="images/i2.jpg" ><img src="images/i2.jpg"  alt="i2" /></a></div>
<div class="amazingcarousel-title">i2</div>
<div class="amazingcarousel-description"></div>
<div class="amazingcarousel-readmore"><a href="images/i2-lightbox.jpg" target="__TARGET__"></a></div>                    </div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
<div class="amazingcarousel-image"><a href="images/i3-lightbox.jpg" title="i3"  class="html5lightbox" data-group="amazingcarousel-3" data-thumbnail="images/i3.jpg" ><img src="images/i3.jpg"  alt="i3" /></a></div>
<div class="amazingcarousel-title">i3</div>
<div class="amazingcarousel-description"></div>
<div class="amazingcarousel-readmore"><a href="images/i3-lightbox.jpg" target="__TARGET__"></a></div>                    </div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
<div class="amazingcarousel-image"><a href="images/b1-lightbox.jpg" title="b1"  class="html5lightbox" data-group="amazingcarousel-3" data-thumbnail="images/b1.jpg" ><img src="images/b1.jpg"  alt="b1" /></a></div>
<div class="amazingcarousel-title">b1</div>
<div class="amazingcarousel-description"></div>
<div class="amazingcarousel-readmore"><a href="images/b1-lightbox.jpg" target="__TARGET__"></a></div>                    </div>
                </li>
            </ul>
            <div class="amazingcarousel-prev"></div>
            <div class="amazingcarousel-next"></div>
        </div>
        <div class="amazingcarousel-nav"></div>
        <div class="amazingcarousel-engine"><a href="http://amazingcarousel.com">JavaScript Carousel</a></div>
    </div>
</div>
<!-- End of body section HTML codes -->

</div>
<!-- End of body section HTML codes -->
