<?php 
	$name = $this->session->userdata('cus_name');
	if($name == ''){
		$home =  base_url();
    	redirect($home);
	}
?>
<!DOCTYPE html>
<html>
<head>
  <title>AFS | <?php echo $name?></title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/w3.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/custom.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="<?php echo base_url()?>js/custom.js"></script>
</head>
<body onload="calcuTotal()">

  <div class="w3-sidebar w3-bar-block w3-card w3-animate-left w3-black" style="display:none;max-width: 300px" id="mySidebar">
    <a href="#" class="w3-bar-item w3-xlarge w3-red nounderline" style="padding: 14px" onclick="w3_close()">Close <i class=" w3-xlarge fa fa-compress"></i></a>

    <?php if($this->uri->segment(2)=='search-food'){?>
	    <a href="<?php echo base_url()?>customer/search-food" class="w3-bar-item w3-large w3-padding-16 nounderline w3-white"><i class="fa fa-search"></i> Search Your Food</a>   
	    <a href="<?php echo base_url()?>customer/history-of-food" class="w3-bar-item w3-large w3-padding-16 nounderline"><i class="fa fa-table"></i> History Of Foods You Tested</a>   
	  <!--   <a href="<?php echo base_url()?>customer/reviewed-food" class="w3-bar-item w3-large w3-padding-16 nounderline"> <i class="fa fa-th-list"></i> History Of Foods You Reviwed</a>
 -->
	<?php } else if($this->uri->segment(2)=='history-of-food'){?> 	
	    <a href="<?php echo base_url()?>customer/search-food" class="w3-bar-item w3-large w3-padding-16 nounderline "><i class="fa fa-search"></i> Search Your Food</a>   
	    <a href="<?php echo base_url()?>customer/history-of-food" class="w3-bar-item w3-large w3-padding-16 nounderline w3-white"><i class="fa fa-table"></i> History Of Foods You Tested</a>   
	  <!--   <a href="<?php echo base_url()?>customer/reviewed-food" class="w3-bar-item w3-large w3-padding-16 nounderline"> <i class="fa fa-th-list"></i> History Of Foods You Reviwed</a>
 -->
	<?php } else if($this->uri->segment(2)=='reviewed-food'){?> 
	    <a href="<?php echo base_url()?>customer/search-food" class="w3-bar-item w3-large w3-padding-16 nounderline "><i class="fa fa-search"></i> Search Your Food</a>   
	    <a href="<?php echo base_url()?>customer/history-of-food" class="w3-bar-item w3-large w3-padding-16 nounderline"><i class="fa fa-table"></i> History Of Foods You Tested</a>   
	    <!-- <a href="<?php echo base_url()?>customer/reviewed-food" class="w3-bar-item w3-large w3-padding-16 nounderline w3-white"> <i class="fa fa-th-list"></i> History Of Foods You Reviwed</a> -->

	<?php } else{?> 
	    <a href="<?php echo base_url()?>customer/search-food" class="w3-bar-item w3-large w3-padding-16 nounderline "><i class="fa fa-search"></i> Search Your Food</a>   
	    <a href="<?php echo base_url()?>customer/history-of-food" class="w3-bar-item w3-large w3-padding-16 nounderline"><i class="fa fa-table"></i> History Of Foods You Tested</a>   
	   <!--  <a href="<?php echo base_url()?>customer/reviewed-food" class="w3-bar-item w3-large w3-padding-16 nounderline"> <i class="fa fa-th-list"></i> History Of Foods You Reviwed</a> -->
	<?php }?> 


  </div>

	<div class="w3-sidebar w3-bar-block w3-black w3-xxlarge w3-animate-left" style="width:70px" id="miniNav">
	  <a href="javascript:void(0)" id="openNav" class="w3-bar-item w3-text-orange" style="margin-top: -4px;" onclick="w3_open()"><i class="fa fa-expand"></i></a>
	  <?php if($this->uri->segment(2)=='search-food'){?>
		  <a href="<?php echo base_url()?>customer/search-food" class="w3-bar-item w3-button w3-white"><i class="fa fa-search"></i></a> 	 
		  <a href="<?php echo base_url()?>customer/history-of-food" class="w3-bar-item w3-button w3-text-orange"><i class="fa fa-table"></i></a> 
		<!--   <a href="<?php echo base_url()?>customer/reviewed-food" class="w3-bar-item w3-button w3-text-orange"><i class="fa fa-th-list"></i> </a>  --> 

	  <?php } else if($this->uri->segment(2)=='history-of-food'){?>
		  <a href="<?php echo base_url()?>customer/search-food" class="w3-bar-item w3-button w3-text-orange"><i class="fa fa-search"></i></a> 	 
		  <a href="<?php echo base_url()?>customer/history-of-food" class="w3-bar-item w3-button w3-white"><i class="fa fa-table"></i></a> 
		<!--   <a href="<?php echo base_url()?>customer/reviewed-food" class="w3-bar-item w3-button w3-text-orange"><i class="fa fa-th-list"></i> </a>   -->

	  <?php } else if($this->uri->segment(2)=='reviewed-food'){?>	 
		  <a href="<?php echo base_url()?>customer/search-food" class="w3-bar-item w3-button w3-text-orange"><i class="fa fa-search"></i></a> 	 
		  <a href="<?php echo base_url()?>customer/history-of-food" class="w3-bar-item w3-button w3-text-orange"><i class="fa fa-table"></i></a> 
		  <!-- <a href="<?php echo base_url()?>customer/reviewed-food" class="w3-bar-item w3-button w3-white"><i class="fa fa-th-list"></i> </a>   -->

	  <?php } else{?>
	      <a href="<?php echo base_url()?>customer/search-food" class="w3-bar-item w3-button w3-text-orange"><i class="fa fa-search"></i></a> 	 
		  <a href="<?php echo base_url()?>customer/history-of-food" class="w3-bar-item w3-button w3-text-orange"><i class="fa fa-table"></i></a> 
		<!--   <a href="<?php echo base_url()?>customer/reviewed-food" class="w3-bar-item w3-button w3-text-orange"><i class="fa fa-th-list"></i> </a>   -->
	 <?php }?> 
	</div>
	 
	<div id="main" style="margin-left: 70px;"> 
	    <div class="">      
	       <!-- Navbar (sit on top) -->
	        <div class="">
	          <div class="w3-bar w3-black" id="navBar">
	            <a class="w3-bar-item w3-button w3-hover-black w3-hide-medium w3-hide-large w3-right w3-margin-top" href="javascript:void(0);" onclick="toggleFunction()" title="Toggle Navigation Menu">
	              <i class="fa fa-bars"></i>
	            </a>            
	            <a href="javascript:void(0)" class="w3-bar-item"><img src="<?php echo base_url()?>img/logo.png" width="55%">
	            </a>

	            <a href="<?php echo base_url()?>/customer/logout" class="w3-bar-item w3-button w3-hide-small w3-hover-green w3-right w3-text-orange" onclick="document.getElementById('restaurentOwner').style.display='block'" style="height:75px;padding-top:25px;"><i class="fa fa-flask"></i> LOGOUT</a>

	            <a href="javascript:void(0)" class="w3-bar-item w3-button w3-hide-small w3-hover-green w3-white w3-right" style="height:75px;padding-top:25px; text-transform: uppercase;" onclick="document.getElementById('foodLover').style.display='block'"><i class="fa fa-glass"></i> <?php echo $this->session->userdata('cus_name')?></a>

	            <a href="<?php echo base_url()?>" class="w3-bar-item w3-button w3-hide-small w3-text-white w3-hover-green w3-right w3-text-orange" style="height:75px;padding-top:25px;"><i class="fa fa-home"></i> HOME</a>

	          </div>

	          <!-- Navbar on small screens -->
	          <div id="navSmall" class="w3-bar-block w3-white w3-hide w3-hide-large w3-hide-medium ">
	            <a href="javascript:void(0)" class="w3-bar-item w3-button" onclick="toggleFunction()">HOME</a>
	            <a href="javascript:void(0)" class="w3-bar-item w3-button" onclick="toggleFunction()">search-food</a>
	            <a href="javascript:void(0)" class="w3-bar-item w3-button" onclick="toggleFunction()">Logout</a>         
	          </div>
	        </div>
	    </div>