<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
// customer
$route['default_controller'] = 'Customer';
$route['customer/search-food'] = 'Customer/showSearchForm';
$route['customer/history-of-food'] = 'Customer/showHisOfFood';
$route['customer/reviewed-food'] = 'Customer/showReviewedFood';

$route['customer/signup'] = 'Customer/doSignup';
$route['customer/login'] = 'Customer/doLogin';
$route['customer/logout'] = 'Customer/doLogout';


// restaurent
$route['restaurent/add-food'] = 'Restaurent/showAddFoodForm';
$route['restaurent/available-food'] = 'Restaurent/showAvailFood';
$route['restaurent/most-ordered-food'] = 'Restaurent/showMostFood';
$route['restaurent/view-pending-order'] = 'Restaurent/showPendingOrder';

$route['restaurent/signup'] = 'Restaurent/doSignup';
$route['restaurent/login'] = 'Restaurent/doLogin';
$route['restaurent/logout'] = 'Restaurent/doLogout';

$route['restaurent/store-food'] = 'Restaurent/doStoreFood';
$route['restaurent/clear-order/(:any)'] = 'Restaurent/doClearOrder';
$route['restaurent/update-food'] = 'Restaurent/doUpdateFood';


// Search
$route['search/result'] = 'Search/showResult';
$route['search/order-details'] = 'Search/showOrderDetails';
$route['search/visit-details'] = 'Search/showVisitDetails';
$route['search/confirm-order'] = 'Search/storeOrder';


$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
