<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();   
        $this->load->model('Customer_Model');       
    }

	public function index()
	{
		$this->load->view('home');
		$this->load->view('template/footer');
	}
	public function showSearchForm(){
		$data['taste'] = $this->Customer_Model->getTaste();
		$data['type'] = $this->Customer_Model->getEnvironment();
		$this->load->view('template/user_profile_nav');		
		$this->load->view('pages/customer/cs_search_food',$data);
		$this->load->view('template/footer');
	}

	public function showHisOfFood(){
		$username = $this->session->userdata('cus_name');
		$data['history'] = $this->Customer_Model->getHistoryOfFood($username);
		$this->load->view('template/user_profile_nav');		
		$this->load->view('pages/customer/cs_his_of_food',$data);
		$this->load->view('template/footer');
	}

	public function showReviewedFood(){
		$this->load->view('template/user_profile_nav');		
		$this->load->view('pages/customer/cs_reviewed_food');
		$this->load->view('template/footer');
	}

	public function doSignup(){
		if($this->Customer_Model->insertSignupData()){
			$this->session->set_userdata('cus_err',"WELCOME");			
			redirect('customer/search-food');
		}
		else{
			$this->session->set_userdata('cus_err',"Ops! Something went wrong!");
			redirect(base_url());
		}
	}

	public function doLogin(){
		if($this->Customer_Model->login()){			
			$this->session->set_userdata('cus_err',"WELCOME BACK");
			redirect('customer/search-food');			
		}else{
			$this->session->set_userdata('cus_err',"Invalid! Try Again");
			redirect(base_url());
		}
	}

	public function doLogout(){
		if($this->Customer_Model->logout()){			
			redirect('customer/search-food');
		}else{			
			redirect(base_url());
		}
	}

}
