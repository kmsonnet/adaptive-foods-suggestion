<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {
	
	public function __construct(){
        parent::__construct();   
        $this->load->model('Search_Model');       
        $this->load->model('Customer_Model');       
    }

	public function showResult(){
		$data['result'] = $this->Search_Model->findFood();
		$data['taste'] = $this->Customer_Model->getTaste();
		$data['type'] = $this->Customer_Model->getEnvironment();
		$this->load->view('template/user_profile_nav');		
		$this->load->view('pages/search/src_search_result',$data);
		$this->load->view('template/footer');
	}

	public function showOrderDetails(){
		$username = $this->session->userdata('cus_name');
		$fid = $this->input->post('fid');
		$data['quan'] = $this->input->post('quantity');
		$data['customer'] = $this->Search_Model->getCustomerDetails($username);
		$data['food'] = $this->Search_Model->getFoodDetails($fid);
		$this->load->view('template/user_profile_nav',$data);		
		$this->load->view('pages/search/src_order_details');
		$this->load->view('template/footer');
	}

	public function showVisitDetails(){
		$data['place'] = $this->input->post('visitadd');
		$data['rname'] = $this->input->post('visitrname');
		$this->load->view('template/user_profile_nav');		
		$this->load->view('pages/search/src_visit_details',$data);
		$this->load->view('template/footer');
	}

	public function storeOrder(){
		$this->Search_Model->insertOrder();
		$data['taste'] = $this->Customer_Model->getTaste();
		$data['type'] = $this->Customer_Model->getEnvironment();
		redirect('customer/search-food');
	}

}
