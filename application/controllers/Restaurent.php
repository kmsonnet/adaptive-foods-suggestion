<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restaurent extends CI_Controller {
	
	public function __construct(){
        parent::__construct();   
        $this->load->model('Restaurent_Model');       
    }

	public function showAddFoodForm(){
		$this->load->view('template/res_profile_nav');		
		$this->load->view('pages/restaurent/rs_add_food');
		$this->load->view('template/footer');
	}

	public function showAvailFood(){
		$rid = $this->session->userdata('res_name');
		$data['foods'] = $this->Restaurent_Model->getFoodDetails($rid);
		$this->load->view('template/res_profile_nav');		
		$this->load->view('pages/restaurent/rs_available_food',$data);
		$this->load->view('template/footer');
	}

	public function showMostFood(){
		$this->load->view('template/res_profile_nav');		
		$this->load->view('pages/restaurent/rs_most_odered');
		$this->load->view('template/footer');
	}

	public function showPendingOrder(){
		$rid = $this->session->userdata('res_name');
		$data['orders'] =$this->Restaurent_Model->getOrders($rid);
		$this->load->view('template/res_profile_nav');		
		$this->load->view('pages/restaurent/rs_pending_order',$data);
		// $this->load->view('template/footer');
	}

	public function doSignup(){
		if($this->Restaurent_Model->insertSignupData()){
			$this->session->set_userdata('res_err',"WELCOME");			
			redirect('restaurent/add-food');
		}
		else{
			$this->session->set_userdata('res_err',"Ops! Something went wrong!");
			redirect(base_url());
		}
	}

	public function doLogin(){
		if($this->Restaurent_Model->login()){			
			$this->session->set_userdata('res_err',"WELCOME BACK");
			redirect('restaurent/add-food');			
		}else{
			$this->session->set_userdata('res_err',"Invalid! Try Again");
			redirect(base_url());
		}
	}

	public function doLogout(){
		if($this->Restaurent_Model->logout()){			
			redirect('restaurent/add-food');
		}else{			
			redirect(base_url());
		}
	}

	public function doStoreFood(){
		if($this->Restaurent_Model->insertFoods()){	
			$this->session->set_userdata('res_err',"Food is Added Successfully");			
			redirect('restaurent/add-food');
		}else {
			$this->session->set_userdata('res_err',"Ops! Something went wrong!");
			redirect(base_url());
		}
	}

	public function doClearOrder(){
		$oid = $this->uri->segment(3);
		$this->Restaurent_Model->clearOrder($oid);
		redirect('restaurent/view-pending-order');
	}

	public function doUpdateFood(){
		$this->Restaurent_Model->updateFood();
		redirect('restaurent/available-food');
	}
}
